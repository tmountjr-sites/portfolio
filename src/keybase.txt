==================================================================
https://keybase.io/tmountjr
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://tommount.net
  * I am tmountjr (https://keybase.io/tmountjr) on keybase.
  * I have a public key ASCIKQGOzcjLfNatcpL1xm34AQYhMMuYkgB2GpZdqkgVhQo

To do so, I am signing this object:

{
    "body": {
        "key": {
            "eldest_kid": "0101a1f5ac5e2542a1f403ad65220c6dbb578c6630f9dd2430dfb499a7bd998f445c0a",
            "host": "keybase.io",
            "kid": "01208829018ecdc8cb7cd6ad7292f5c66df801062130cb989200761a965daa4815850a",
            "uid": "839053346a527190f01e64b2d40d6219",
            "username": "tmountjr"
        },
        "service": {
            "hostname": "tommount.net",
            "protocol": "https:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "client": {
        "name": "keybase.io go client",
        "version": "1.0.20"
    },
    "ctime": 1490357315,
    "expire_in": 504576000,
    "merkle_root": {
        "ctime": 1490357260,
        "hash": "b152b3503e84b31f10af30793c59b51d647c4745ff1c7aa8d4557159e365de754fefc74fa0a58f66a5e93d0012345838299849646a7f093412ed72a1de2735c9",
        "seqno": 976272
    },
    "prev": "f9e6f1cf2129c8cd7c3cdccea2409025d424e1f4e6d963fe8ef8859ad0a67264",
    "seqno": 17,
    "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgiCkBjs3Iy3zWrXKS9cZt+AEGITDLmJIAdhqWXapIFYUKp3BheWxvYWTFAvV7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTAxYTFmNWFjNWUyNTQyYTFmNDAzYWQ2NTIyMGM2ZGJiNTc4YzY2MzBmOWRkMjQzMGRmYjQ5OWE3YmQ5OThmNDQ1YzBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwODgyOTAxOGVjZGM4Y2I3Y2Q2YWQ3MjkyZjVjNjZkZjgwMTA2MjEzMGNiOTg5MjAwNzYxYTk2NWRhYTQ4MTU4NTBhIiwidWlkIjoiODM5MDUzMzQ2YTUyNzE5MGYwMWU2NGIyZDQwZDYyMTkiLCJ1c2VybmFtZSI6InRtb3VudGpyIn0sInNlcnZpY2UiOnsiaG9zdG5hbWUiOiJ0b21tb3VudC5uZXQiLCJwcm90b2NvbCI6Imh0dHBzOiJ9LCJ0eXBlIjoid2ViX3NlcnZpY2VfYmluZGluZyIsInZlcnNpb24iOjF9LCJjbGllbnQiOnsibmFtZSI6ImtleWJhc2UuaW8gZ28gY2xpZW50IiwidmVyc2lvbiI6IjEuMC4yMCJ9LCJjdGltZSI6MTQ5MDM1NzMxNSwiZXhwaXJlX2luIjo1MDQ1NzYwMDAsIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNDkwMzU3MjYwLCJoYXNoIjoiYjE1MmIzNTAzZTg0YjMxZjEwYWYzMDc5M2M1OWI1MWQ2NDdjNDc0NWZmMWM3YWE4ZDQ1NTcxNTllMzY1ZGU3NTRmZWZjNzRmYTBhNThmNjZhNWU5M2QwMDEyMzQ1ODM4Mjk5ODQ5NjQ2YTdmMDkzNDEyZWQ3MmExZGUyNzM1YzkiLCJzZXFubyI6OTc2MjcyfSwicHJldiI6ImY5ZTZmMWNmMjEyOWM4Y2Q3YzNjZGNjZWEyNDA5MDI1ZDQyNGUxZjRlNmQ5NjNmZThlZjg4NTlhZDBhNjcyNjQiLCJzZXFubyI6MTcsInRhZyI6InNpZ25hdHVyZSJ9o3NpZ8RAKKjeIFMHL2FrKT9n4BtvOQjA6FV0HOet9VJLloLHFLxtyiXhWQqUHcpYds63oIuMQW+pn0UlSZfaHAEBTTR0DqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEINtIE5TAx4GPV+H7MJ1RTYy75ocn7ujIX7+W0nWVEIIDo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/tmountjr

==================================================================