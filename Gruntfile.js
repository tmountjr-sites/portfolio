module.exports = function(grunt) {

    var fs = require('fs');
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        config: {
            path: {
                root: '.',
                src: '<%= config.path.root %>/src',
                spriteSrc: '<%= config.path.root %>/sprite-src',
                retinaSpriteSrc: '<%= config.path.spriteSrc %>/retina',
                img: '<%= config.path.src %>/img',
                public: '<%= config.path.root %>/public',
            }
        },

        sprite: {
            all: {
                src: '<%= config.path.spriteSrc %>/*.png',
                retinaSrcFilter: '<%= config.path.spriteSrc %>/*@2x.png',
                dest: '<%= config.path.img %>/sprites.png',
                retinaDest: '<%= config.path.img %>/sprites@2x.png',
                destCss: '<%= config.path.src %>/_sprites.scss',
            },
        },

        pngmin: {
            compile: {
                options: {
                    ext: ".png",
                    force: true,
                },
                files: [{
                    src: '<%= config.path.img %>/*.png',
                    dest: '<%= config.path.img %>'
                }],
            }
        },

        clean: {
            sprite: [
                '<%= config.path.img %>/sprites.png',
                '<%= config.path.src %>/_sprites.scss',
            ],
        },

        watch: {
            options: {
                livereload: false
            },
            sprites: {
                files: ['<%= config.path.spriteSrc %>/*.png'],
                tasks: ['clean', 'sprite', 'pngmin']
            },
            otherPng: {
                files: ['<%= config.path.img %>/*.png', '!<%= config.path.img %>/sprites.png'],
                tasks: ['pngmin']
            },
            articles: {
                files: ['<%= config.path.src %>/articles/_data.json'],
                tasks: ['createArticles', 'createTagPages']
            }
        }

    });

    grunt.registerTask('createWorkPages', 'Create Work pages based on the contents of _data.json', function() {
        var _data = grunt.file.readJSON('./src/_data.json');
        for (var customer in _data) {
            if (! grunt.file.exists('./src/' + customer + '.jade')) {
                grunt.file.write('./src/' + customer + '.jade', "extends _work\n");
                grunt.log.oklns('Created new file: ./src/' + customer + '.jade');
            } else {
                grunt.log.writeln('File ./src/' + customer + '.jade already exists; skipping.');
            }
        }
    });

    grunt.registerTask('createArticles', 'Create Article pages based on the contents of _data.json', function() {
        var _data = grunt.file.readJSON('./src/articles/_data.json');
        for (var article in _data) {
            if (! grunt.file.exists('./src/articles/' + article + '.jade')) {
                grunt.file.write('./src/articles/' + article + '.jade', "extends _article\n");
                grunt.log.oklns('Created new file: ./src/articles/' + article + '.jade');
            } else {
                grunt.log.writeln('File ./src/articles/' + article + '.jade already exists; skipping.');
            }
        }
    });

    grunt.registerTask('createTagPages', 'Create pages for the /tags/ directory based on article tags', function() {
        var _data = grunt.file.readJSON('./src/articles/_data.json'),
            tags = {};

        for (var article in _data) {
            for (var i = 0; i < _data[article].tags.length; i++) {
                var articleDetails = _data[article];
                if (! tags.hasOwnProperty(articleDetails.tags[i])) {
                    tags[articleDetails.tags[i]] = {"slugs": []};
                }
                tags[articleDetails.tags[i]].slugs.push(article);
            }
        }

        grunt.file.write('./src/tags/_data.json', JSON.stringify(tags));
        grunt.log.oklns('Wrote new ./src/tags/_data.json.');

        for (var tagName in tags) {
            if (! grunt.file.exists('./src/tags/' + tagName + '.jade')) {
                grunt.file.write('./src/tags/' + tagName + '.jade', "extends _tag")
                grunt.log.oklns('Created new file: ./src/tags/' + tagName + '.jade');
            } else {
                grunt.log.writeln('File ./src/tags/' + tagName + '.jade already exists; skipping.');
            }
        }
    });

    grunt.registerTask('dev', [
        'build',
        'watch',
    ]);

    grunt.registerTask('build', [
        'clean',
        'sprite',
        'createWorkPages',
        'createArticles',
        'createTagPages',
        'pngmin',
    ]);
}
